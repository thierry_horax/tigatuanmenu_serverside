<?php header("Access-Control-Allow-Origin:*");?>
<?php 
    date_default_timezone_set("Asia/Jakarta");
    $con = new mysqli("localhost","id11085793_root","tigatuan","id11085793_resto_menu");
    $nomor_meja = $_POST['nomor_meja'];
    $nomor_nota = date("Y") . date("m") . date("d") . date("H") . date("i") . date("s") . $nomor_meja;
    $username = $_POST['username'];
    $diskon = json_decode($_POST['diskon'],true);
    $tanggal= date("Y/m/d H:i:s");
    $itemInCart = json_decode($_POST['cart'],true);
    $besar_diskon =0;
    $id_diskon = 0;
    $is_diskon = false;
    $menuYangStockKosong= array();
    $menuYangStockTidakCukup = array();

    for($i=0;$i<count($itemInCart);$i++)
    {
        $sqlCheckStock = "select * from menu where idmenu='" . $itemInCart[$i]['idmenu'] . "'";
        $res = $con->query($sqlCheckStock);
        $row = $res->fetch_assoc();
        if($row['stock']==0)
        {
            array_push($menuYangStockKosong,$row);
        }
        else if($row['stock'] < $itemInCart[$i]['jumlahOrder'] && $row['stock']!=0)
        {
            array_push($menuYangStockTidakCukup,$row);
        
        }
    }

    if($diskon != "tidak ada diskon")
    {
        $id_diskon = $diskon[0]['iddiskon'];
        $besar_diskon = $diskon[0]['besar_diskon'];
        $is_makanan_diskon = $diskon[0]['is_makanan_diskon'];
        $is_minuman_diskon = $diskon[0]['is_minuman_diskon'];
    }

    if(count($menuYangStockKosong) == 0 && count($menuYangStockTidakCukup)== 0)
    {
        $sql = "INSERT INTO nota (nomor_nota,tanggal_transaksi,nomor_meja,status,username) values ('". $nomor_nota . "','" . $tanggal ."','" . $nomor_meja. "','". 'Pesanan telah diterima' . "','" . $username  ."')";
        

        if($con->query($sql) === true)  
        {
            for($i=0;$i<count($itemInCart);$i++)
            {
                $is_diskon=true;
                $idmenu = $itemInCart[$i]['idmenu'];
                $jumlahOrder = $itemInCart[$i]['jumlahOrder'];
                $hargaSatuan = $itemInCart[$i]['hargaSatuan'];
                $permintaanKhusus = $itemInCart[$i]['permintaan_khusus'];
                $sql2 = "";
                if($id_diskon == 0)
                {
                    $sql2= "INSERT INTO rincian_nota (nomor_nota,idmenu,quantity,harga_sebelum_diskon,harga_sesudah_diskon,referensi_customer) values ('". $nomor_nota ."','" . $idmenu ."','" . $jumlahOrder ."','" . $hargaSatuan ."','" . $hargaSatuan ."','" . $permintaanKhusus  ."')";
                    $con->query($sql2);
                }
                else
                {
                    if($itemInCart[$i]['jenis'] =="makanan")
                    {
                        if($is_makanan_diskon == 1)
                        {
                            $harga_setelah_diskon = $hargaSatuan - ($besar_diskon / 100 * $hargaSatuan);
                            $sql2= "INSERT INTO rincian_nota (nomor_nota,idmenu,quantity,harga_sebelum_diskon,harga_sesudah_diskon,referensi_customer,diskon_iddiskon) values ('". $nomor_nota ."','" . $idmenu ."','" . $jumlahOrder ."','" . $hargaSatuan ."','" . $harga_setelah_diskon ."','" . $permintaanKhusus ."','" . $id_diskon ."')";
                            $con->query($sql2);
                        }
                        else
                        {
                            $is_diskon = false;
                        }
                    }
                    else if($itemInCart[$i]['jenis']=="minuman")
                    {
                        if($is_minuman_diskon == 1)
                        {
                            $harga_setelah_diskon = $hargaSatuan - ($besar_diskon / 100 * $hargaSatuan);
                            $sql2= "INSERT INTO rincian_nota (nomor_nota,idmenu,quantity,harga_sebelum_diskon,harga_sesudah_diskon,referensi_customer,diskon_iddiskon) values ('". $nomor_nota ."','" . $idmenu ."','" . $jumlahOrder ."','" . $hargaSatuan ."','" . $harga_setelah_diskon ."','" . $permintaanKhusus ."','" . $id_diskon ."')";
                            $con->query($sql2);
                        }
                        else
                        {
                            $is_diskon = false;
                        }
                    }

                    if($is_diskon == false)
                    {
                        $sql2= "INSERT INTO rincian_nota (nomor_nota,idmenu,quantity,harga_sebelum_diskon,harga_sesudah_diskon,referensi_customer) values ('". $nomor_nota ."','" . $idmenu ."','" . $jumlahOrder ."','" . $hargaSatuan ."','" . $hargaSatuan ."','" . $permintaanKhusus  ."')";
                        $con->query($sql2); 
                    }
                }

                $stock =0;
                $sql3 = "select stock from menu where idmenu='" . $idmenu ."'";
                $res = $con->query($sql3);
                while($row=$res->fetch_assoc())
                {
                    $stock=((int)$row['stock']) - $jumlahOrder;
                }
                
                $sql4="update menu set stock='" . $stock ."' where idmenu='" . $idmenu ."'";
                $con->query($sql4);
            }
            
            echo json_encode('success');
            
        }
    }
    else if(count($menuYangStockKosong) == 0 && count($menuYangStockTidakCukup) > 0)
    {
        echo json_encode($menuYangStockTidakCukup);
    }
    else if(count($menuYangStockKosong) >= 1 && count($menuYangStockTidakCukup)>=1)
    {
        echo json_encode(array_merge($menuYangStockKosong,$menuYangStockTidakCukup));
    }
    else if(count($menuYangStockKosong) >= 1 && count($menuYangStockTidakCukup)==0)
    {
        echo json_encode($menuYangStockKosong);
    }

    /*$result = array();
        for($i =0 ; $i<count($menuYangStockKosong); $i++)
        {
            array_push($result,$menuYangStockKosong[$i]);
        }

        for($j=0; $j<count($menuYangStockTidakCukup);$i++)
        {
            array_push($result,$menuYangStockTidakCukup[$j]);
        }
        echo json_encode("konichiwa"); */
?>