<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('daftarorder');
})->name('home');

Route::get('/daftardiskon', function () {
    return view('daftardiskon');
})->name('diskon.daftardiskon');

Route::post('/menu/{id}/ubahstock', 'MenuController@ubahStock')->name('menu.ubahstock');
Route::get('/username/{username}/checkusername', 'UsernameController@checkUsernameIsAvaiable')->name('username.checkusername');
Route::get('/order/getallorder', 'OrderController@getAllOrder')->name('order.getallorder');
Route::get('/order/{nomor_nota}/detailorder', 'OrderController@getOrderDetail')->name('order.orderdetail');
Route::get('/order/{nomor_nota}/{jsondata}/updateOrder', 'OrderController@updateOrder')->name('order.updateOrder');
Route::get('/order/{nomor_nota}/lihatDetailOrder', 'OrderController@getOrderDetailForModal')->name('order.lihatdetailorder');
Route::get('/transaksi/penjualanhariini', 'TransaksiController@getPenjualanHariIni')->name('transaksi.penjualanhariini');
Route::get('transaksi/penjualankeseluruhan', 'TransaksiController@getPenjualanKeseluruhan')->name('transaksi.keseluruhan');
Route::get('/diskon/{iddiskon}/gantistatus', 'DiskonController@changeStatusDiskon')->name('diskon.ubahstatusdiskon');
Route::get("/diskon/getalldiskon", "DiskonController@getAllDiskon")->name("diskon.getalldiskon");
Route::get('/print/{nomor_nota}', 'OrderController@print')->name('order.print');
Route::get('/ubahstatusnota/{nomor_nota}', 'OrderController@ubahStatusNota')->name('order.ubahstatusnota');
Route::get('/checkkategoriisavaible/{nama_kategori}', "KategoriController@isKategoriAda")->name("kategori.checkkategori");
Route::get('/cancelorder/{nomor_nota}', 'OrderController@cancelOrder')->name("order.cancelorder");
Route::get('/confirmationorder/{nomor_nota}', 'OrderController@selesaikanPesanan')->name("order.selesaikanpesanan");
Route::resource('menu', 'MenuController');
Route::resource('kategori', 'KategoriController');
Route::resource('username', 'UsernameController');
Route::resource('order', 'OrderController');
Route::resource('diskon', 'DiskonController');
