<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
    protected $table = "nota";
    protected $primaryKey = "nomor_nota";
    public $timestamps = false;
}
