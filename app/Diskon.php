<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diskon extends Model
{
    protected $table="diskon";
    protected $primaryKey="iddiskon";
    public $timestamps = false;
    protected $fillable= ['nama_kategori'];
}
