<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TransaksiController extends Controller
{
    public function getPenjualanHariIni()
    {
        date_default_timezone_set("Asia/Jakarta");
        $tanggal_hari_ini = date('Y-m-d');
        $penjualan = DB::table('nota')->where('tanggal_transaksi', 'like', "%" . $tanggal_hari_ini . "%")->orderBy('tanggal_transaksi', 'DESC')->get();

        return view('transaksipenjualanhariini', compact('penjualan'));
    }
    public function getPenjualanKeseluruhan()
    {
        $penjualan = DB::table('nota')->orderBy('tanggal_transaksi', 'DESC')->get();
        //dd($penjualan);
        return view('transaksipenjualankeseluruhan', compact('penjualan'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
