<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;
use DB;

class KategoriController extends Controller
{
    public function isKategoriAda($argNamaKategori)
    {
        $isSame = false;
        $kategori = Kategori::all();
        for ($i = 0; $i < count($kategori); $i++) {
            if (strtolower($kategori[$i]['nama_kategori']) == strtolower($argNamaKategori)) {
                $isSame = true;
                break;
            }
        }

        if ($isSame == false) {
            return json_encode(['status' => "available"]);
        } else {
            return json_encode(['error' => "unavailable"]);
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allkategori = Kategori::all();
        return view('daftarkategori', compact('allkategori'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahkategori');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kategori = new Kategori();
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->save();

        return redirect()->route('kategori.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        $kategori->delete();
        return redirect()->back();
    }
}
