<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Menu;
use App\Kategori;
use DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ubahStock(Request $request, $id)
    {
        $menu = Menu::find($id);
        $menu->stock = $request->stock;
        $menu->save();

        return redirect()->route('menu.index');
    }
    public function index()
    {
        $allmenu = Menu::all();
        return view('daftarmenu', compact('allmenu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allKategori = Kategori::all();
        return view('tambahmenu', compact('allKategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menu = new Menu();
        $menu->nama_menu = $request->nama_menu;

        $menu->harga = str_replace('.', '', str_replace('Rp. ', '', $request->harga));
        $menu->stock = $request->stock;
        $menu->keterangan_menu = $request->keterangan_menu;
        $menu->idkategori = $request->idkategori;
        $menu->jenis = $request->jenis;
        if ($request->is_pedas == 1) {
            $menu->is_pedas = $request->is_pedas;
        } else {
            $menu->is_pedas = 0;
        }
        $menu->save();


        if ($request->hasFile('picture')) {
            $getLastIdInserted = DB::table('menu')->select('idmenu')->orderBy('idmenu', 'desc')->limit(1)->get();
            $getLastIdInserted = json_decode($getLastIdInserted, true);
            //var_dump($getLastIdInserted);
            $image = $request->file('picture');
            $ext = $image->getClientOriginalExtension();
            $name = $request->nama_menu . '.' . $ext;
            $destinationPath = public_path('../resources/assets/images_menu');
            $image->move($destinationPath, $name);
            DB::table("extension_gambar")->insert(['ext' => $ext, 'idmenu' => $getLastIdInserted[0]['idmenu']]);
        }

        return redirect()->route('menu.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allKategori = Kategori::all();
        $detailmenu = Menu::find($id);

        return view('editmenu', compact('detailmenu', 'allKategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $menu = Menu::find($id);
        $old_menu_name = $menu->nama_menu;
        $menu->nama_menu = $request->nama_menu;
        $menu->harga = str_replace('.', '', str_replace('Rp. ', '', $request->harga));
        $menu->keterangan_menu = $request->keterangan_menu;
        $menu->idkategori = $request->idkategori;
        $menu->jenis = $request->jenis;
        if ($request->is_pedas == 1) {
            $menu->is_pedas = $request->is_pedas;
        } else {
            $menu->is_pedas = 0;
        }
        $path = '../resources/assets/images_menu';
        foreach (glob($path . '/*.*') as $file) {

            $filedetail = pathinfo($file);

            if ($filedetail['filename'] == $old_menu_name) {
                rename($file, $path . "/" . $request->nama_menu . '.' . $filedetail['extension']);
                break;
            }
        }

        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $ext = $image->getClientOriginalExtension();
            $name = $request->nama_menu . '.' . $ext;
            $destinationPath = public_path('../resources/assets/images_menu');
            $image->move($destinationPath, $name);

            DB::table("extension_gambar")->where('idmenu', $id)->update(['ext' => $ext]);
        }



        $menu->save();

        return redirect()->route('menu.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('extension_gambar')->where('idmenu', $id)->delete();
        $menu = Menu::find($id);
        $menu->delete();

        $path = '../resources/assets/images_menu';
        foreach (glob($path . '/*.*') as $file) {

            $filedetail = pathinfo($file);

            if ($filedetail['filename'] == $menu->nama_menu) {
                unlink($file);

                break;
            }
        }

        return redirect()->back();
    }
}
