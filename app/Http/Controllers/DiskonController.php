<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Diskon;
use Session;
use Redirect;
use DB;

class DiskonController extends Controller
{
    public function getAllDiskon()
    {
        $diskon = Diskon::all();
        return json_encode($diskon);
    }
    public function changeStatusDiskon($iddiskon)
    {
        $diskon = Diskon::find($iddiskon);

        if ($diskon->status == "Tidak Berlaku") {

            $is_any_diskon_active = "";
            $nama_diskon_yang_aktif = "";
            $alldiskon = Diskon::all();
            for ($i = 0; $i < count($alldiskon); $i++) {
                if ($alldiskon[$i]->status == "Berlaku") {
                    $is_any_diskon_active = "ada";
                    $nama_diskon_yang_aktif = $alldiskon[$i]->nama_diskon;
                    break;
                }
            }

            if ($is_any_diskon_active == "ada") {
                return json_encode($nama_diskon_yang_aktif);
            } else if ($is_any_diskon_active != "ada") {
                $diskon->status = "Berlaku";
                $diskon->save();
                return json_encode("aktif");
            }
        } else {
            $diskon->status = "Tidak Berlaku";
            $diskon->save();
            return json_encode("nonaktif");
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldiskon = Diskon::all();
        return view('daftardiskon', compact('alldiskon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahdiskon');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $diskon = new Diskon();
        $diskon->judul_diskon = $request->judul_diskon;
        $diskon->keterangan_diskon = $request->keterangan_diskon;



        $waktuberlaku = explode('-', $request->waktu_berlaku);
        $diskon->waktu_berlaku_diskon = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', trim($waktuberlaku[0]) . ":00")));
        $diskon->waktu_berakhir_diskon = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', trim($waktuberlaku[1]) . ":00")));
        $diskon->besar_diskon = $request->besar_diskon;
        $diskon->status = 'Tidak Berlaku';

        if ($request->is_makanan_berlaku == 1) {
            $diskon->is_makanan_diskon = $request->is_makanan_berlaku;
        } else {
            $diskon->is_makanan_diskon = 0;
        }

        if ($request->is_minuman_berlaku == 1) {
            $diskon->is_minuman_diskon = $request->is_minuman_berlaku;
        } else {
            $diskon->is_minuman_diskon = 0;
        }

        $diskon->save();

        if ($request->hasFile('picture')) {
            $getLastIdInserted = DB::table('diskon')->select('iddiskon')->orderBy('iddiskon', 'desc')->limit(1)->get();
            $getLastIdInserted = json_decode($getLastIdInserted, true);
            $image = $request->file('picture');
            $ext = $image->getClientOriginalExtension();
            $name = $request->judul_diskon . '.' . $ext;
            $destinationPath = public_path('../resources/assets/images_promosi');
            $image->move($destinationPath, $name);
            DB::table("extension_gambar_promosi")->insert(['ext' => $ext, 'iddiskon' => $getLastIdInserted[0]['iddiskon']]);
        }

        return redirect()->route('diskon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $diskon = Diskon::find($id);
        return view('editdiskon', compact('diskon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $diskon = Diskon::find($id);
        $old_judul_diskon = $diskon->judul_diskon;
        $diskon->judul_diskon = $request->judul_diskon;
        $diskon->keterangan_diskon = $request->keterangan_diskon;

        $waktuberlaku = explode('-', $request->waktu_berlaku);
        $diskon->waktu_berlaku_diskon = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', trim($waktuberlaku[0]) . ":00")));
        $diskon->waktu_berakhir_diskon = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', trim($waktuberlaku[1]) . ":00")));
        $diskon->besar_diskon = $request->besar_diskon;


        $path = '../resources/assets/images_promosi';
        $list_file = glob($path . '/*.*');
        if (count($list_file) != 0) { }
        foreach (glob($path . '/*.*') as $file) {

            $filedetail = pathinfo($file);

            if ($filedetail['filename'] == $old_judul_diskon) {
                rename($file, $path . "/" . $request->judul_diskon . '.' . $filedetail['extension']);
                break;
            }
        }



        if ($request->is_makanan_berlaku == 1) {
            $diskon->is_makanan_diskon = $request->is_makanan_berlaku;
        } else {
            $diskon->is_makanan_diskon = 0;
        }

        if ($request->is_minuman_berlaku == 1) {
            $diskon->is_minuman_diskon = $request->is_minuman_berlaku;
        } else {
            $diskon->is_minuman_diskon = 0;
        }

        $diskon->save();

        if ($request->hasFile('picture')) {
            $image = $request->file('picture');
            $ext = $image->getClientOriginalExtension();
            $name = $request->judul_diskon . '.' . $ext;
            $destinationPath = public_path('../resources/assets/images_promosi');
            $image->move($destinationPath, $name);
            DB::table("extension_gambar_promosi")->where('iddiskon', $id)->update(['ext' => $ext]);
        }

        return redirect()->route('diskon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('extension_gambar_promosi')->where('iddiskon', $id)->delete();
        $diskon = Diskon::find($id);
        $diskon->delete();

        $path = '../resources/assets/images_promosi';
        foreach (glob($path . '/*.*') as $file) {

            $filedetail = pathinfo($file);

            if ($filedetail['filename'] == $diskon->judul_diskon) {
                unlink($file);
                break;
            }
        }

        return redirect()->back();
    }
}
