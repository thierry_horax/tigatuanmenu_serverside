<?php

namespace App\Http\Controllers;

use Mike42\Escpos\PrintConnectors;
use Mike42\Escpos\Printer;
use Illuminate\Http\Request;
use DB;
use App\Nota;
use App\Order;
use App\RincianNota;
use Response;
use Input;

class OrderController extends Controller
{
    public function selesaikanPesanan($argNomorNota)
    {
        $order = Nota::find($argNomorNota);
        $order->status = "Pesanan telah diantar";
        $order->save();
        return json_encode($order);
    }

    public function cancelOrder($argNomorNota)
    {
        $order = Nota::find($argNomorNota);
        $order->status = "Pesanan dibatalkan";
        $order->save();
        return json_encode($order);
    }
    public function ubahStatusNota($argNomorNota)
    {
        $nota = Nota::find($argNomorNota);
        $nota->status = "Pesanan sedang disiapkan";
        $nota->save();

        return "done";
    }
    public function print($argNomorNota)
    {
        $detailnota = DB::table('nota')->select('nota.nomor_nota', 'nota.nomor_meja', 'menu.nama_menu', 'rincian_nota.quantity', 'rincian_nota.referensi_customer')->join('rincian_nota', 'nota.nomor_nota', '=', 'rincian_nota.nomor_nota')->join('menu', 'rincian_nota.idmenu', '=', 'menu.idmenu')->where('nota.nomor_nota', '=', $argNomorNota)->get();
        $nota = Nota::find($argNomorNota);
        $nota->status = "Pesanan sedang disiapkan";
        $nota->save();
        //dd($detailnota);
        return view('print', compact('detailnota'));
    }
    public function updateOrder($nomor_nota, $jsondata)
    {
        $orderdetail = DB::table('nota')->select('rincian_nota.idmenu', 'menu.nama_menu', 'rincian_nota.quantity', 'rincian_nota.referensi_customer')->join('rincian_nota', 'rincian_nota.nomor_nota', '=', 'nota.nomor_nota')->join('menu', 'menu.idmenu', '=', 'rincian_nota.idmenu')->where('rincian_nota.nomor_nota', '=', $nomor_nota)->get();
        $result = json_decode($jsondata, true);
        $orderdetail = json_decode(json_encode($orderdetail), true);

        foreach ($orderdetail as $value) {
            $array_temp1[] = $value['idmenu'];
        }

        foreach ($result as $value) {
            $array_temp2[] = $value['idmenu'];
        }
        $removed_array = array_diff($array_temp1, $array_temp2);
        if (count($removed_array) > 1) {
            foreach ($removed_array as $value) {

                DB::table('rincian_nota')->where('idmenu', '=', $value)->where('nomor_nota', '=', $nomor_nota)->delete();
            }
        } else if (count($removed_array) == 1) {
            DB::table('rincian_nota')->where('idmenu', '=', $removed_array)->where('nomor_nota', '=', $nomor_nota)->delete();
        } else if (count($removed_array) == 0) {
            for ($i = 0; $i < count($result); $i++) {
                DB::table('rincian_nota')->where('nomor_nota', '=', $nomor_nota)->where('idmenu', '=', $result[$i]['idmenu'])->update(['quantity' => $result[$i]['quantity'], 'referensi_customer' => $result[$i]['referensi_konsumen']]);
            }
        }



        return "done";
    }
    public function getAllOrder()
    {
        $order = DB::table('nota')->where('nota.status', '=', 'Pesanan telah diterima')->orWhere('nota.status', '=', 'Pesanan sedang disiapkan')->get();

        return json_encode($order);
    }

    public function getOrderDetail($nomor_nota)
    {
        $orderdetail = DB::table('nota')->select('menu.nama_menu', 'rincian_nota.quantity', 'rincian_nota.referensi_customer')->join('rincian_nota', 'rincian_nota.nomor_nota', '=', 'nota.nomor_nota')->join('menu', 'menu.idmenu', '=', 'rincian_nota.idmenu')->where('rincian_nota.nomor_nota', '=', $nomor_nota)->get();
        //dd($orderdetail);
        return json_encode($orderdetail);
    }

    /*select m.idmenu,m.nama_menu,rn.referensi_customer,rn.quantity
from menu m inner join rincian_nota rn on m.idmenu = rn.idmenu inner join nota n on rn.nomor_nota = n.nomor_nota 
where n.nomor_nota = '201908220840474'*/

    public function getOrderDetailForModal($nomor_nota)
    {
        $orderdetail = DB::table('menu')->select('menu.idmenu as idmenu', 'rn.referensi_customer as referensi', 'menu.nama_menu as nama_menu', 'rn.quantity as quantity')->join('rincian_nota as rn', 'rn.idmenu', '=', 'menu.idmenu')->join('nota as n', 'n.nomor_nota', '=', 'rn.nomor_nota')->where('n.nomor_nota', '=', $nomor_nota)->get();
        return json_encode($orderdetail);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    { }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { }
}
