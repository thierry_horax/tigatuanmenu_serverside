<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Restoran Tiga Tuan</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('bower_components/Ionicons/css/ionicons.min.css')}}">

  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">

  <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

      <!-- Logo -->
      <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">RTT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Restoran Tiga Tuan</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="{{asset('../resources\assets\images/logo.jpg')}}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>Admin</p>
            <!-- Status -->
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>

        <!-- search form (Optional) -->
        <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form>!-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
          <!-- Optionally, you can add icons to the links -->
          <li class="active"><a href="{{route('home')}}"><i class="fa fa-link"></i> <span>Daftar Order</span></a></li>
          <li class=" treeview">
            <a href="#">
              <i class="fa fa-link"></i> <span>Transaksi</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-left"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class=""><a href="{{route('transaksi.penjualanhariini')}}"><i class="fa"></i>Penjualan Hari Ini</a></li>
              <li><a href="{{route('transaksi.keseluruhan')}}"><i class="fa"></i>Penjualan Keseluruhan</a></li>
            </ul>
          </li>
          <li class=" treeview">
            <a href="#">
              <i class="fa fa-link"></i> <span>Menu</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class=""><a href="{{route('menu.index')}}"><i class="fa"></i>Daftar Menu</a></li>
              <li><a href="{{route('menu.create')}}"><i class="fa"></i>Tambah Menu</a></li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-link"></i> <span>Kategori</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class=""><a href="{{route('kategori.index')}}"><i class="fa"></i>Daftar Kategori</a></li>
              <li><a href="{{route('kategori.create')}}"><i class="fa"></i>Tambah Kategori</a></li>
            </ul>
          </li>
          <li class=" treeview">
            <a href="#">
              <i class="fa fa-link"></i> <span>Diskon</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="active"><a href="{{route('diskon.index')}}"><i class="fa"></i>Daftar Diskon</a></li>
              <li><a href="{{route('diskon.create')}}"><i class="fa"></i>Tambah Diskon</a></li>
            </ul>
          </li>
          <li class=""><a href="{{route('username.create')}}"><i class="fa fa-link"></i> <span>Buat Akun</span></a></li>
        </ul>
        <!-- /.sidebar-menu -->
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Main content -->
      <section class="content container-fluid">

        <!--------------------------
        | Your Page Content Here |
        -------------------------->
        <div class="" style="">
          <div class="col-md-3 kolom-order" style="overflow: hidden; width:1000px;">

          </div>
          <!-- /.col -->
        </div>


      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="modalnotifikasi" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header title">

            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body isi"></div>
          <div class="modal-footer button">

          </div>
        </div>
      </div>
    </div>

    <div class="modal fade" id="editOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"">
      <div class=" modal-dialog" role="document">
      <div class="modal-content" style="width:800px">
        <div class=" modal-header">
          <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body">
          <div class=" box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title nomor_nota"></h3>
              <input type="hidden" class="nomor_nota_hidden" value="">
            </div>
            <div class="box-body table-editable">
              <table style="table-layout:fixed;" class="table table-bordered table-responsive-md table-striped text-center table_order_detail">
                <thead>
                  <tr>
                    <th style="width:150px;" class="text-center">Nama Menu</th>
                    <th style="width:40px;" class="text-center">Qty</th>
                    <th class="text-center">Referensi Customer</th>
                    <th style="width:100px;" class="text-center">Action</th>
                  </tr>
                </thead>
                <tbody class="table_content">

                </tbody>
              </table>
              <div class="form-group">
                <button onclick="SaveOrder()" class="btn btn-primary btn-simpan" value="Simpan">Simpan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


  </div>
  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">Company</a>.</strong> All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane active" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="pull-right-container">
                  <span class="label label-danger pull-right">70%</span>
                </span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
</body>
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js">
  $(document).ready(function() {
    $('#example').DataTable();
  });
</script>



<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Morris.js charts -->
<script src="{{asset('bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script>
  var tempArray = [];
  var table = $('#daftar-menu').DataTable();
  var isSame = true;
  var detailNota = [];
  var slice = [];
  var tempArrayOrderDetail = [];
  setInterval(getAllOrder, 1000);


  $('document').ready(function() {
    getAllOrder();
  });


  function displayOrder(argListOrder) {

    for (var i = 0; i < argListOrder.length; i++) {
      var nomor_nota = argListOrder[i]['nomor_nota'];
      $('.kolom-order').append('<div class="box box-primary order-detail" style="float:left; width:500px;"><div class="box-header with-border"><h3 class="box-title nomor-nota ' + nomor_nota + '" value="' + nomor_nota + '"></h3><div class="box-tools pull-right"><button class="btn btn-box-tool btn-print"  onclick="print(' + nomor_nota + ')" title="print"><i class="fa fa-print"></i></button><button type="button" class="btn btn-box-tool btn-open-modal" title="Edit" onclick="DisplayOrderDetailToModal(' + nomor_nota + ')" data-target="#editOrderModal" data-toggle="modal"><i class="fa fa-pencil"></i></button><button class="btn btn-box-tool btn_cancel" onclick="notifikasiBatalNota(' + nomor_nota + ')" title="Batalkan pesanan"><i class="fa fa-close"></i></button><button class="btn btn-box-tool btn_done" onclick="notifikasiSelesaikanPesanan(' + nomor_nota + ')" title="Selesaikan pesanan"><i class="fa fa-check"></i></button></div></div><div class="box-body body-' + nomor_nota + '"></div></div>');
      $('.' + nomor_nota).html(nomor_nota + " - " + argListOrder[i]['username']);
      displayOrderDetail("body-" + nomor_nota, nomor_nota);

    }
  }

  function notifikasiSelesaikanPesanan(argNomorNota) {
    $(".button").html('<button class="btn btn-primary button_hapus" type="button" onclick="selesaikanOrder(' + argNomorNota + ')" data-dismiss="modal">Selesai</button>');
    $(".title").html('Peringatan');
    $('.isi').html("Apakah anda yakin ingin menyelesaikan pesanan dengan nomor nota " + argNomorNota + " ?");
    $('#modalnotifikasi').modal();
  }

  function notifikasiBatalNota(argNomorNota) {
    $(".button").html('<button class="btn btn-danger button_hapus" type="button" onclick="cancelOrder(' + argNomorNota + ')" data-dismiss="modal">Hapus</button>');
    $(".title").html('Peringatan');
    $('.isi').html("Apakah anda yakin ingin membatalkan pesanan dengan nomor nota " + argNomorNota + " ?");
    $('#modalnotifikasi').modal();
  }

  function selesaikanOrder(argNomorNota) {
    var urlFormat = "{{ route('order.selesaikanpesanan', ['nomor_nota' => 'VAR_ID']) }}";
    urlFormat = urlFormat.replace('VAR_ID', argNomorNota);
    $.ajax({
      type: "GET",
      url: urlFormat,
      headers: {
        "X-CSRF-TOKEN": "{{csrf_token()}}"
      },
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function(data) {
        console.log(data);

      }
    });
  }

  function cancelOrder(argNomorNota) {
    var urlFormat = "{{ route('order.cancelorder', ['nomor_nota' => 'VAR_ID']) }}";
    urlFormat = urlFormat.replace('VAR_ID', argNomorNota);
    $.ajax({
      type: "GET",
      url: urlFormat,
      headers: {
        "X-CSRF-TOKEN": "{{csrf_token()}}"
      },
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function(data) {
        console.log(data);

      }
    });
  }

  function displayOrderDetail(argNamaClassBody, argNomorNota) {
    var isSame = true;
    var urlFormat = "{{ route('order.orderdetail', ['nomor_nota' => 'VAR_ID']) }}";
    urlFormat = urlFormat.replace('VAR_ID', argNomorNota);
    $.ajax({
      type: "GET",
      url: urlFormat,
      headers: {
        "X-CSRF-TOKEN": "{{csrf_token()}}"
      },
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function(data) {
        detailNota = JSON.parse(data);
        if ($('.' + argNamaClassBody).html().trim() == false) {
          for (var j = 0; j < detailNota.length; j++) {
            $('.' + argNamaClassBody).append("<p>" + detailNota[j]['nama_menu'] + " - " + detailNota[j]['quantity'] + "</p>");
          }
        } else {
          var resultFromDatabase = JSON.parse(data);

          $('.' + argNamaClassBody).empty();
          for (var j = 0; j < resultFromDatabase.length; j++) {
            $('.' + argNamaClassBody).append("<p>" + resultFromDatabase[j]['nama_menu'] + " - " + resultFromDatabase[j]['quantity'] + "</p>");
          }
        }
      }
    });
  }

  function SaveOrder() {
    var nomor_nota = $('.nomor_nota_hidden').val();
    var data_order = [];
    var count_rows = $(".table_order_detail tr").length - 1;
    for (var j = 0; j < detailNota.length; j++) {
      data_order.push({
        idmenu: $('.idmenu' + detailNota[j]['idmenu']).val(),
        quantity: $('.quantity' + detailNota[j]['idmenu']).text(),
        referensi_konsumen: $(".referensi" + detailNota[j]['idmenu']).text()
      });
    }

    console.log(data_order);
    var urlFormat = "{{ route('order.updateOrder', ['nomor_nota' => 'VAR_ID','jsondata' => 'JSON_DATA']) }}";
    urlFormat = urlFormat.replace('VAR_ID', nomor_nota);
    urlFormat = urlFormat.replace('JSON_DATA', JSON.stringify(data_order));
    $.ajax({
      type: "get",
      url: urlFormat,
      headers: {
        "X-CSRF-TOKEN": "{{csrf_token()}}"
      },
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function(data) {
        if (data == 'done') {
          alert('Order pada nota ' + nomor_nota + ' telah diubah.');
          displayOrderDetail('body-' + nomor_nota, nomor_nota);
        }
      }
    });
  }

  function removeMenuFromListOrder(argIndexItemRemoved, argIdMenu) {
    for (var i = 0; i < detailNota.length; i++) {
      if (detailNota[i]['idmenu'] == argIdMenu) {
        detailNota.splice(i, 1);
        break;
      }
    }
    $(".item_menu" + argIndexItemRemoved).remove();
  }

  function DisplayOrderDetailToModal(argNomorNota) {
    var urlFormat = "{{ route('order.lihatdetailorder', ['nomor_nota' => 'VAR_ID']) }}";
    urlFormat = urlFormat.replace('VAR_ID', argNomorNota);
    $.ajax({
      type: "GET",
      url: urlFormat,
      headers: {
        "X-CSRF-TOKEN": "{{csrf_token()}}"
      },
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function(data) {
        detailNota = JSON.parse(data);
        console.log(detailNota);
        $('.table_content').html('');
        $('.nomor_nota').html('Ubah Rincian Order - ' + argNomorNota);
        $('.nomor_nota_hidden').val(argNomorNota);
        for (var i = 0; i < detailNota.length; i++) {
          $('.table_content').append('<tr class="item_menu' + i + '"><input type="hidden" class="idmenu' + detailNota[i]['idmenu'] + '" value="' + detailNota[i]['idmenu'] + '"><td><lable value="' + detailNota[i]['nama_menu'] + '">' + detailNota[i]['nama_menu'] + '</lable></td><td contenteditable="true"><lable class="quantity' + detailNota[i]['idmenu'] + '" value="' + detailNota[i]['quantity'] + '">' + detailNota[i]['quantity'] + '</lable></td><td class="referensi' + detailNota[i]['idmenu'] + '" style="word-wrap: break-word" contenteditable="true">' + detailNota[i]['referensi'] + '</td><td><button onclick="removeMenuFromListOrder(' + i + ',' + detailNota[i]['idmenu'] + ' )" class="btn btn-primary button_remove">Remove</button></td></tr>');
        }

      }
    });
  }

  function getAllOrder() {

    var urlFormat = "{{ route('order.getallorder') }}";
    $.ajax({
      type: "GET",
      url: urlFormat,
      headers: {
        "X-CSRF-TOKEN": "{{ csrf_token() }}"
      },
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      success: function(data) {
        var resultFromDatabase = JSON.parse(data);
        if (tempArray.length == 0) {
          tempArray = resultFromDatabase;
        }
        tempArray = tempArray.concat().sort();
        resultFromDatabase = resultFromDatabase.concat().sort();
        if (tempArray.length < resultFromDatabase.length || tempArray.length > resultFromDatabase.length) {
          $('.kolom-order').empty();
          tempArray = resultFromDatabase;
          displayOrder(resultFromDatabase);
        } else if (tempArray.length == resultFromDatabase.length) {
          for (var i = 0; i < tempArray.length; i++) {
            if (tempArray[i]['nomor_nota'] == resultFromDatabase[i]['nomor_nota']) {
              isSame = true;
            } else if (tempArray[i] != resultFromDatabase[i]) {
              isSame = false;
              break;
            }
          }
          if (isSame == false) {
            console.log('tidak sama');
          } else {
            if ($('.kolom-order').html().trim() == false) {
              displayOrder(resultFromDatabase);
            }
          }
        }
      }
    });
  }

  function print(argNomorNota) {
    var url = "{{route('order.print',':nomor_nota')}}";
    url = url.replace(':nomor_nota', argNomorNota);
    window.location.href = url;

  }
</script>


</html>