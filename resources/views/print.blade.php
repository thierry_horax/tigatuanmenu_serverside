<script type="text/JavaScript" src="{{asset('style.scss')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/JavaScript" src="{{asset('js/jqueryprint/jQuery.print.js')}}"></script>
<a class="goback" href="{{URL::previous()}}">
	<h2>Kembali ke halaman utama</h2>
</a>
<style>
	@media print {
		#invoice-POS {
			margin: 0;
			height: 100%;
			padding: 0;

		}

		.goback {
			visibility: hidden;

		}
	}
</style>
<div id="invoice-POS">

	<center id="top">
		<div class="logo"></div>
		<div class="info">
			<h2>Tiga Tuan</h2>
			<h2>Kitchen & Bar</h2>
		</div>
		<!--End Info-->
	</center>
	<!--End InvoiceTop-->

	<div id="mid">
		<div class="info">
			<table>
				<tr>
					<td>No. Nota </td>
					<td> : </td>
					<td>{{$detailnota[0]->nomor_nota}} </td>
					<td><input type="hidden" class="nomor_nota" value="{{$detailnota[0]->nomor_nota}}"></td>
				</tr>
				<tr>
					<td>No. Meja</td>
					<td> : </td>
					<td>{{$detailnota[0]->nomor_meja}} </td>
				</tr>
			</table>
		</div>
	</div>
	<!--End Invoice Mid-->
	<div>----------------------------------------------------</div>
	<div id="bot">

		<div id="table">
			<table>
				<tr class="tabletitle">
					<td class="item" style="width: 100%"><b>Item<b></td>
					<td class="Hours"><b>Qty</b></td>
				</tr>
				@for($i=0;$i<count($detailnota); $i++) <tr class="tabletitle">
					<td class="item">{{$detailnota[$i]->nama_menu}}</td>
					<td class="Hours">{{$detailnota[$i]->quantity}}</td>
					</tr>
					<tr class="tabletitle">
						<td class="item" style="word-wrap:break-word"><label style="font-size: 8pt">Catatan : {{$detailnota[$i]->referensi_customer}}</label></td>
						<td class="Hours"></td>
					</tr>
					@endfor
			</table>
		</div>
		<!--End Table-->
	</div>
	<!--End InvoiceBot-->
	<br>
	<br>

</div>
<!--End Invoice-->
<br>
<script>
	/*$('document').ready(function(){
		$('#invoice-POS').print({
			addGlobalStyles : true,
			stylesheet : null,
			rejectWindow : true,
			noPrintSelector : ".no-print",
			iframe : true,
			append : null,
			prepend : null
			});
	});*/
	window.onload = function() {
		window.print();

	};

	/*$('.action-button').click(function(){
		ubahstatusnota();
		console.log('tes');
	});
	function ubahstatusnota()
	{
		var argNomorNota = $(".nomor_nota").val();
		console.log(argNomorNota);
		var urlFormat = "{{ route('order.ubahstatusnota', ['nomor_nota' => 'VAR_ID']) }}";
		urlFormat = urlFormat.replace('VAR_ID', argNomorNota);
		$.ajax({
		type: "GET",
		url : urlFormat,
		headers: { "X-CSRF-TOKEN" : "{{ csrf_token() }}" },
		processData: false,
		contentType: false,
		cache: false,
		timeout: 600000,
		success: function (data)
		{
			
		}
		
		});
		
	}*/
</script>